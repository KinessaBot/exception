﻿using System;

namespace Except
{
    class Worker
    {
        private string name;
        public string Name
        {
            set
            {
                foreach (char c in value)
                    if (Char.IsLetter(c))
                        name = value;
                    else throw new NameException("Name must contain only letters", value);
            }
            get
            {
                return name;
            }
        }
        public int Age
        {
            set
            {
               Age = value;
            }
            get
            {
                return Age;
            }
        }

        public Worker()
        {

        }
        public Worker(string name, int a)
        {
            Name = name;
            Age = a;
        }
    }
}
